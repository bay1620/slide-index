# Slide index
This micro service enables you to upload pdf slides (e.g. lecture slides) and query them. 
It adheres to the standard request format of the [Related Items project](https://gitlab.rrz.uni-hamburg.de/bay1620/wilps-related-items/).

# Setup
Pull the image. Replace the version number. If unknown look up the latest version here: https://hub.docker.com/r/fwelter/wilps_slide_index

`docker pull fwelter/wilps_slide_index:<VERSION_NUMBER>`

Run the image. Adjust the ports according to your setup. Most likly the containers needs to be put [behind an 
nginx](https://gitlab.rrz.uni-hamburg.de/bay1620/wilps-related-items/#nginx) or another webserver to ensure SSL encryption.

```
sudo docker run -p 8080:5000 \
--env EXTERNAL_HOST=<YOUR_EXTERNAL_DOMAIN_OR_ENDPOINT> \
-v /home/<YOURUSER>/slide-index/img_cache:/app/img_cache \
-v /home/<YOURUSER>/slide-index/index:/app/index \
-v /home/<YOURUSER>/slide-index/slides:/app/slides \
-dit \
fwelter/wilps_slide_index:<VERSION_NUMBER>
```

The volume mounts are optional. If left out the container will start without content on every restart. 

# Usage
After starting the slide index and browsing to the page
you will see the following four sections.

## Upload new PDF files
Select one or more PDF files that should be added to the index.  
Also specify the line number which contains the title.  
This process can take some time, since each pdf page is also
converted to an image.

## Indexed files
Gives an overview of already indexed files. 
Also you can get an excerpt of the extracted titles. 
If these are not the correct titles, reupload the slides
and specify the correct line number during the upload. 

## Query
This sections enables you to test the indexed files
and see which pdf pages are returned for a given query.

## Upload benchmark data
Using an evaluation file (see `benchmark_data/bidm/terms.csv` for an example)
one can check the search performance of the index.

