from search_index.basic_search_index import BasicSearchIndex


class CherryPickTitleSearchIndex(BasicSearchIndex):
    """Return fitting slides, favouring slides with the query contained in the title"""

    def __init__(self, booster_words=None):
        self.booster_words = booster_words or ["concept", "definition"]
        super().__init__()

    def search(self, query, context):
        results = self.result_list(query, context)

        # Look for result with query and booster word in the title
        for result in results:
            if query.lower() in result["title"].lower() \
                    and any([bw in result["title"].lower() for bw in self.booster_words]):
                return result

        for result in results:
            if query.lower() in result["title"].lower():
                return result
        return results
