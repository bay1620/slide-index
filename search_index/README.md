# Search index
This folder contains different implementations of a search index. 
`BasicSearchIndex` uses the [Whoosh](https://whoosh.readthedocs.io/en/latest/intro.html)
library under the hood. All other classes inherit from `BasicSearchIndex`. 
However different implementation are possible, as long as they adhere to 
the interface implicitly defined by `BasicSearchIndex`.

Currently `TitleFocusSearchIndex` is in use for the application.
This is configurable in [app.py](https://gitlab.rrz.uni-hamburg.de/bay1620/slide-index/-/blob/master/app.py#L18) around line 18.

## Weighting 
Finding the right slide for a given query is a typical informations retrieval task.
While in the web context a vast amount of ranking signals can be used, with PDF
files only text and possibly a title are available. However it also means that
we do not need to watch out for black hat SEO measures. Therefore we can
fully trust the title and use it as the primary indicator for matching slides.

If the slide index is deployed in a larger production setup, functionality for
user feedback could be employed to find more accurate matches for reoccuring 
queries. 
