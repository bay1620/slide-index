from deprecated import deprecated
from nltk import word_tokenize
from whoosh.qparser import QueryParser, OrGroup

from .basic_search_index import BasicSearchIndex


@deprecated(reason="Uses overly complicated query creation. Use TitleFocusSearchIndex instead.")
class ContextAwareSearchIndex(BasicSearchIndex):
    """Return fitting slides based on context and title"""

    def result_list(self, query, context):
        term = query + ""  # Make new object
        query = " ".join([token + "^1" for token in word_tokenize(term)])
        context_tokens = word_tokenize(context)
        if len(context_tokens) > 0:
            individual_context_token_weight = str(round(1 / len(context_tokens), 3))
            query += " " + " ".join([token + "^" + individual_context_token_weight for token in context_tokens])
        query_parser = QueryParser("content", self.ix.schema, group=OrGroup.factory(0.9))
        return self.ix.searcher().search(query_parser.parse(query))
