from whoosh.qparser import QueryParser, OrGroup

from nltk.corpus import stopwords

from .basic_search_index import BasicSearchIndex
from .german_stopwords import stopwords as german_stopwords


def clean_context(term, context):
    context = context.split(" ")
    # Remove words from context that are already in the term
    for token in term.split(" "):
        if token.strip() != "":
            try:
                context.remove(token)
            except ValueError:
                pass

    # Remove stop words
    for lang in ["english", "german"]:
        for stopword in stopwords.words(lang):
            try:
                context.remove(stopword)
            except ValueError:
                pass
    # for stopword in german_stopwords:
    #    context = context.replace(stopword, "")

    return " ".join(context)


class TitleFocusSearchIndex(BasicSearchIndex):
    """Return fitting slides, boosting terms contained in the title"""

    def result_list(self, query, context):
        user_query = query
        context = clean_context(user_query, context)
        query = "title:({})^2".format(user_query)
        query += " content:({})^1.2".format(user_query)
        if len(context.strip()) > 0:
            query += " title:({})^0.2".format(context)
            query += " content:({})^0.1".format(context)
        query_parser = QueryParser("content", self.ix.schema, group=OrGroup.factory(0.9))
        parsed_query = query_parser.parse(query)
        return self.ix.searcher().search(parsed_query)
