# Slide indexer

This folder contains classes responsible for processing a PDF file and adding it
to an index. The current `BasicIndexer` needs to know the index and the image
dir. Furthermore the line number which contains the title can be configured (default: 0).
