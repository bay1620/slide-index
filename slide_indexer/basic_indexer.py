import os

import pdfplumber


class BasicIndexer():
    """Add a single pdf file to the index"""

    def __init__(self, index, image_dir):
        self.index = index
        self.image_dir = image_dir
        self.process_images = True
        self.title_row = 0

    def skip_images(self):
        self.process_images = False

    def add_pdf(self, pdf_file_path):
        pdf = pdfplumber.open(pdf_file_path)
        pdf_file_name = pdf_file_path.split(os.sep)[-1]
        for i, page in enumerate(pdf.pages):
            text = page.extract_text()
            print(self.title_row)
            self.index.add(pdf_file_name, i + 1, text, text.split("\n")[self.title_row])
            if self.process_images:
                img_name = pdf_file_name + "_" + str(i + 1) + ".jpg"
                img_path = os.path.join(self.image_dir, img_name)
                page.to_image().save(img_path)
