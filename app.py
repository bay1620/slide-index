import glob
import os
from pathlib import Path

from flask import Flask, render_template, request, redirect, send_file, jsonify, flash
from werkzeug.utils import secure_filename

from benchmark import Benchmark
from search_index import TitleFocusSearchIndex
from slide_indexer import BasicIndexer

app = Flask(__name__)

SLIDE_DIR = "slides"
IMAGE_DIR = "img_cache"
INDEX_DIR = "index"

Index = TitleFocusSearchIndex


@app.route('/')
def index():
    ll = os.listdir(SLIDE_DIR)
    ll.sort()
    return render_template('index.html', ll=ll)


@app.route('/current_index')
def current_index():
    index = Index(index_dir=INDEX_DIR)
    return "<br>".join(
        ["{} {} {}".format(res["file_name"], res["page"], res["title"]) for res in index.search("*", "")])


def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ["pdf"]


def benchmark_allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ["csv"]


@app.route('/upload', methods=['POST'])
def upload():
    if 'files' in request.files:
        files = request.files.getlist('files')
        index = Index(index_dir=INDEX_DIR)
        indexer = BasicIndexer(index, IMAGE_DIR)
        if request.form['title_row']:
            indexer.title_row = int(request.form['title_row'])
        for i, file in enumerate(files):
            if file.filename != '':
                if file and allowed_file(file.filename):
                    filename = secure_filename(file.filename)
                    file_path = os.path.join(Path(SLIDE_DIR), filename)
                    file.save(file_path)
                    indexer.add_pdf(file_path)
        del indexer
        del index
    return redirect('/')


@app.route("/search", methods=['POST'])
def query():
    index = Index(index_dir=INDEX_DIR)
    query = request.form.get("term")
    context = request.form.get("context")
    amount = int(request.form.get("amount"))
    results = index.search(query, context)[:amount]
    if len(results) == 0:
        return jsonify({
            "type": "miss"
        })
    full_path = lambda res: os.getenv('EXTERNAL_HOST', '<PLEASE_SET_EXTERNAL_HOST_ENV_VAR>') + \
                            "/slide/" + res["file_name"] + "_" + str(res["page"]) + ".jpg"
    return jsonify({
        "type": "image",
        "paths": [{"path": full_path(res), "url": full_path(res)} for res in results]
    })


@app.route("/reset_index", methods=['POST'])
def reset_index():
    for folder in [IMAGE_DIR, SLIDE_DIR, INDEX_DIR]:
        files = glob.glob(folder + '/*')
        for f in files:
            os.remove(f)
    return redirect('/')


@app.route("/slide/<img_name>")
def slide(img_name):
    path = os.path.join(IMAGE_DIR, img_name)
    if "/" in img_name or "\\" in img_name:
        return "bad image name", 400
    if not os.path.exists(path):
        return "image not present", 404
    return send_file(path, mimetype='image/jpg')


@app.route("/benchmark", methods=["POST"])
def benchmark():
    if 'file' not in request.files:
        flash('No file part')
        return redirect(request.url)
    file = request.files['file']
    if file.filename == '':
        flash('No selected file')
        return redirect(request.url)
    if file and benchmark_allowed_file(file.filename):
        benchmark = Benchmark(file)
        benchmark.run()
        return benchmark.result().to_html() + '<br><a href="/">Zurück</a>'
    flash('Wrong file format')
    return redirect(request.url)


if __name__ == '__main__':
    app.debug = True
    app.run(host="0.0.0.0")
