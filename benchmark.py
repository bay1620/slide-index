import pandas as pd
import requests


class Benchmark:
    def __init__(self, file):
        self.terms = pd.read_csv(file, dtype={"term": str, "context": str, "file": str, "page": int})
        self.benchmark_results = []

    def run(self):
        terms = self.terms
        for i in terms.index:
            response = requests.post("http://localhost:5000/search",
                                     {"term": terms["term"][i], "context": terms["context"][i]})
            json = response.json()
            if json["type"] == "miss":
                score = 0
                img_name = "miss"
            if json["type"] == "image":
                img_name = json["path"].split("/")[-1]
                should_be_img_name = terms["file"][i] + "_" + str(terms["page"][i]) + ".jpg"
                if img_name == should_be_img_name:
                    score = 1
                else:
                    score = 0.1
            self.benchmark_results.append((
                score, img_name, should_be_img_name, terms["term"][i], terms["context"][i]
            ))

    def result(self):
        return pd.DataFrame(self.benchmark_results, columns=["score", "img_name", "should_be_img_name", "term", "context"])
