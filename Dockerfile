FROM python:3.8

RUN pip install pdfplumber Whoosh Flask nltk Deprecated pandas requests
RUN python -m nltk.downloader punkt && python -m nltk.downloader stopwords
RUN apt-get update
RUN apt-get install -y libmagickwand-dev ghostscript
RUN rm /etc/ImageMagick-6/policy.xml
COPY policy.xml /etc/ImageMagick-6/policy.xml
# Remove imagemagick policy from /etc/ImageMagick-6/policy.xml if pdf2img needed

RUN mkdir app && mkdir app/slides && mkdir app/index && mkdir app/img_cache
COPY . app
WORKDIR app
#RUN mkdir slides && mkdir index && mkdir img_cache
#RUN sh allow_pdf_wand.sh


EXPOSE 5000

ENV FLASK_APP=app.py
ENV SERVER_NAME=0.0.0.0

ENTRYPOINT flask run -h 0.0.0.0